<?php
	require_once('lib/app.php');
	 
?>
<!DOCTYPE html>
<html>
<head>
	<title>create</title>
</head>
<body>
<form action="add.php" method="post">
	<fieldset>
		<legend>User info</legend>
		<!-- name -->
		<div>
			<label for="txtFullName">Full name</label>
			<input type="text" name="full_name" id="txtFullName"value="">
		</div>
		<!-- password -->
		<div>
			<label for="password">Password</label>
			<input type="password" name="password" id="password"value="">
		</div>


		<!-- gender -->
		<div>
			<label>Gender</label>
			<input type="Radio" name="gender" id="optGender1" value="male" checked="checked">
			<label for="optGender1">Male</label>

			<input type="Radio" name="gender" id="optGender2" value="female">
			<label for="optGender2">Female</label>
		</div>

		<!-- city -->
		<div>
			<label for="city">Select City</label>
			<select name="city" id="city">
				<option disabled="disabled" selected="selected">Select A city</option>
				<option value="Chittagong">Chittagong</option>
				<option value="Dhaka">Dhaka</option>
				<option value="Sylhet">Sylhet</option>
				<option value="Rajshahi">Rajshahi</option>
			</select>
		</div>
		<!-- hobbies -->
		<div>
			<label >Select Your Hobbies</label>
			<p><input type="checkbox" name="hobbies[]" value="boat"> I enjoy boat journey</p>
			<p><input type="checkbox" name="hobbies[]" value="coding"> I enjoy coadind</p>
			<p><input type="checkbox" name="hobbies[]" value="gardening"> I enjoy gardening</p>
			<p><input type="checkbox" name="hobbies[]" value="chess"> I enjoy chess</p>
		</div>

		<!-- about me -->
		<div>
			<label for="about_me">About me</label>
			<textarea name="about_me" id="about_me"></textarea>
		</div>

		<input type="submit" value="Save Info">
	</fieldset>
</form>

</body>
</html>