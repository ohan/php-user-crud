<?php
	require_once('lib/app.php');
	$query = "SELECT * FROM user";
	$result = mysqli_query($link, $query);
	$users = array();
	while($row = mysqli_fetch_assoc($result)){
		//$row['hobbies'] = explode(',',$row['hobbies']);
		$users[] = $row;
	}
	 //dd($users);
?>
<!DOCTYPE html>
<html>
<head>
	<title>List of users</title>
</head>
<body>
<!-- messages -->
<?php if(isset($_SESSION['msg_success']) AND !empty($_SESSION['msg_success'])):?>
	<h4 style="color:green"><?php echo $_SESSION['msg_success']; unset($_SESSION['msg_success'])?></h4>
<?php endif; ?>
<?php if(isset($_SESSION['msg_error']) AND !empty($_SESSION['msg_error'])):?>
	<h4 style="color:red"><?php echo $_SESSION['msg_error']; unset($_SESSION['msg_error'])?></h4>
<?php endif; ?>
<!-- ./end of msg -->
<nav>
	<li><a href="create.php">create new user</a></li>
</nav>
<h1>List of Users</h1>
<table border="1">
	<thead>
		<tr>
			<th>id</th>
			<th>name</th>
			<th>hobbies</th>
			<th>gender</th>
			<th>city</th>
			<th>about_me</th>
			<th>password</th>
			<th>actions</th>
		</tr>
	</thead>
	<tbody>
	<?php if(count($users)):?>
		<?php foreach($users as  $user):?>
		<tr>
			<td><?php echo $user['id'] ?></td>
			<td><?php echo $user['full_name'] ?></td>
			<td><?php echo $user['hobbies'] ?></td>
			<td><?php echo $user['gender'] ?></td>
			<td><?php echo $user['city'] ?></td>
			<td><?php echo $user['about_me'] ?></td>
			<td><?php echo $user['password'] ?></td>
			<td>
				<a href="delete.php?id=<?php echo $user['id'] ?>"> delete</a> |
				<a href="view.php?id=<?php echo $user['id'] ?>">view</a> |
				<a href="edit.php?id=<?php echo $user['id'] ?>">Edit</a>

			</td>
		</tr>
		<?php endforeach;?>
	<?php else:?>
		<tr><td colspan="4">No user found!</td></tr>
	<?php endif;?>
	</tbody>
</table>
</body>
</html>