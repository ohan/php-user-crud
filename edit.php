<?php
	require_once('lib/app.php');
	$id = $_GET['id'];
	// $user = $_SESSION['users'][$id];
	// $full_name = $user['full_name'];
	// $hobbies = $user['hobbies'];

	$query = "SELECT * FROM user WHERE id=".$id;
	$result = mysqli_query($link, $query);
	$user = array();
	while($row = mysqli_fetch_assoc($result)){
		$row['hobbies'] = explode(",", $row['hobbies']);
		$user = $row;

	}
	$hobbies = $user['hobbies'];
	$full_name = $user['full_name']
	 
?>

<!DOCTYPE html>
<html>
<head>
	<title>List of registered users</title>
</head>
<body>
<form action="modify.php?id=<?php echo $id;?>" method="post">
	<fieldset>
		<legend>Gender Info</legend>
		<!-- name -->
		<div>
			<label for="txtFullName">Full name</label>
			<input type="text" name="full_name" id="txtFullName"value="<?php echo $user['full_name'];?>">
		</div>
		<!-- hobbies -->
		<div>
			<label >Select Your Hobbies</label>
			<p><input type="checkbox" name="hobbies[]" value="boat" <?php if(in_array('boat', $hobbies)){echo "checked";}?>> I enjoy boat journey</p>
			<p><input type="checkbox" name="hobbies[]" value="coding" <?php if(in_array('coding', $hobbies)){echo "checked";}?>> I enjoy coadind</p>
			<p><input type="checkbox" name="hobbies[]" value="gardening" <?php if(in_array('gardening', $hobbies)){echo "checked";}?>> I enjoy gardening</p>
			<p><input type="checkbox" name="hobbies[]" value="chess" <?php if(in_array('chess', $hobbies)){echo "checked";}?>> I enjoy chess</p>
		</div>
		<!-- password -->
		<div>
			<label for="password">Password</label>
			<input type="password" name="password" id="password"value="<?php echo $user['password'];?>">
		</div>


		<div>
			<label>Gender</label>
			<input type="Radio" name="gender" id="optGender1" value="male" <?php if($user['gender'] == 'male'){echo 'checked';}?> />
			<label for="optGender1">Male</label>

			<input type="Radio" name="gender" id="optGender2" value="female" <?php if($user['gender'] == 'female'){echo 'checked';}?>>
			<label for="optGender2">Female</label>
		</div>

		<!-- city -->
		<div>
			<label for="city">Select City</label>
			<select name="city" id="city">
				<option value="Chittagong" <?php if($user['city'] == "Chittagong") echo "selected";?>>Chittagong</option>
				<option value="Dhaka" <?php if($user['city'] == "Dhaka") echo "selected";?> >Dhaka</option>
				<option value="Sylhet" <?php if($user['city'] == "Sylhet") echo "selected";?>>Sylhet</option>
				<option value="Rajshahi" <?php if($user['city'] == "Rajshahi") echo "selected";?>>Rajshahi</option>
			</select>
		</div>

		<input type="submit" value="Save Info">
	</fieldset>
</form>

</body>
</html>